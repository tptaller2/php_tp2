<?php
require_once('crud_integrantes.php');
require_once('crud_estados.php');
require_once('estados.php');
require_once('integrantes.php');
require_once('Tareas.php');
$crudIntegrantes= new CrudIntegrantes();
$crudEstados= new CrudEstados();
$tarea= new Tareas();
$estados= new estados();
$integrantes= new integrantes();
$listaIntegrantes=$crudIntegrantes->mostrar();
$listaEstados=$crudEstados->mostrar();
?>
<html>
<head>
	<title>Ingresar Tarea</title>
</head>
<header>
Ingresa descripción de la tarea
</header>
<form action='tareas_controller.php' method='post'>
	<table>
<!-- 		<tr>
			<td>Nombre tarea:</td>
			<td> <input type='text' name='id_tarea'></td>
		</tr> -->
		<tr>
			<td>Fecha Tarea:</td>
			<td><input type='date' name='fecha_tarea' ></td>
		</tr>
		<tr>
			<td>Duración	:</td>
			<td><input type='number' name='duracion_tarea' ></td>
		</tr>
		<tr>
			<td>Descripción:</td>
			<td><input type='text' name='desc_tarea' ></td>
		</tr>
		<tr>
			<td>Integrante:</td>
			<td>
				<select name="id_integrante"> 
					<?php foreach ($listaIntegrantes as $integrantes) {?>
						<option value="<?php echo $integrantes->getId_integrante()?>"><?php echo($integrantes->getNombre()." ".$integrantes->getApellido()) ?></option>
					<?php }?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Estado:</td>
			<td>
				<select name="estado">
					<?php foreach ($listaEstados as $estados) {?>
						<option value="<?php echo $estados->getId_estado()?>"><?php echo($estados->getDescripcion()) ?></option>
					<?php }?>
				</select>
			</td>
			<tr>
			<td>Observaciones:</td>
			<td><input type='text' name='observaciones'></td>
		</tr>
		</tr>
		<input type='hidden' name='insertar' value='insertar'>
	</table>
	<input type='submit' value='Guardar'>
	<a href="index.php">Volver</a>
</form>
 
</html>