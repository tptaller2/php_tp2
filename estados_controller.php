<?php
require_once('crud_estados.php');
require_once('estados.php');

$crud= new CrudEstados();
$estado= new estados();

	if (isset($_POST['insertar'])) {
		$estado->setDescripcion($_POST['descripcion']);

		$crud->insertar($estado);
		header('Location: index.php');

	}elseif(isset($_POST['actualizar'])){
		$estado->setId_estado($_POST['id_estado']);
		$estado->setDescripcion($_POST['descripcion']);
		$crud->actualizar($estado);
		header('Location: ingresar_estado.php');

	}elseif ($_GET['accion']=='e') {
		$crud->eliminar($_GET['id_estado']);
		header('Location: ingresar_estado.php');		

	}elseif($_GET['accion']=='a'){
		header('Location: ingresar_estado.php');
	}
?>