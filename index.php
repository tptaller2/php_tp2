<?php
require_once('crud_integrantes.php');
require_once('crud_tareas.php');
require_once('crud_estados.php');
require_once('estados.php');
require_once('integrantes.php');
require_once('Tareas.php');
$crud = new CrudTareas();
$crudIntegrantes = new CrudIntegrantes();
$crudEstados = new CrudEstados();
$tarea = new Tareas();
$estados = new estados();
$integrantes = new integrantes();
$listaIntegrantes = $crudIntegrantes->mostrar();
$listaEstados = $crudEstados->mostrar();
$listaTareas = $crud->mostrar();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="style.css" />
  <link rel="shortcut icon" href="./img/consulta.ico" type="image/x-icon" />
  <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet" />
  <title>Task Management Tool</title>
</head>

<body data-spy="scroll" data-target="#info-nav" class="d-flex flex-column min-vh-100">
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <a class="navbar-brand" href="#">Task Management Tool</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#info-nav" aria-controls="info-nav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="info-nav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="#dynamic-container" onclick="showForm('member')">Ingresar Integrante</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#dynamic-container" onclick="showForm('status')">Ingresar Estado</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#dynamic-container" onclick="showForm('task')">Ingresar Tarea</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#dynamic-container" onclick="showForm('board')">Ver Tareas</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="container-fluid">
    <div class="row d-flex justify-content-center introduccion">
      <div class="row" id="home">
        <div class="col-12 fondo-titulo">
          <div class="titulo">Task Management Tool v1.0</div>
        </div>
      </div>
      <div class="col-12">
        <div class="col-md-8 col-lg-7 texto-intro ml-2 mr-2 mt-5 slide-animate-left primer-linea">
          Bienvenido a la herramienta de tareas TMT<br />
          Desde aquí podrá dar seguimiento a las tareas necesarias para llevar
          a cabo su proyecto
          <br /><br />
        </div>
      </div>
    </div>

    <div class="col-12 d-flex justify-content-around panel-home align-items-center" id="panel">
      <h1>Panel de Control</h1>
    </div>
    <div class="panel-botones row">
      <div class="col-sm-3 d-flex align-items-center">
        <button class="btn-lg btn-primary full-width" onclick="showForm('board')">
          Ver tareas
        </button>
      </div>
      <div class="col-sm-3 d-flex align-items-center">
        <button class="btn-lg btn-primary full-width" onclick="showForm('member')">
          Ingresar integrante
        </button>
      </div>
      <div class="col-sm-3 d-flex align-items-center">
        <button class="btn-lg btn-primary full-width" onclick="showForm('status')">
          Ingresar estado
        </button>
      </div>
      <div class="col-sm-3 d-flex align-items-center">
        <button class="btn-lg btn-primary full-width" onclick="showForm('task')">
          Ingresar Tarea
        </button>
      </div>
    </div>

    <section class="row" id="dynamic-container">

      <form class="contenedor-tablas col-sm-6 " action="integrantes_controller.php" method="post" id="insertarIntegrantes" style="display: none;">
        <div class="form-group">
          <label for="nombre">Nombre</label>
          <input input type="text" name="nombre" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="apellido">Apellido</label>
          <input input type="text" name="apellido" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="mail">Mail</label>
          <input input type="email" name="mail" class="form-control" required>
        </div>
        <input type="hidden" name="insertar" value="insertar" />
        <input type="submit" class="btn btn-primary" value="Guardar" />
      </form>

      <form class="contenedor-tablas col-sm-6" action="estados_controller.php" method="post" id="insertarEstado" style="display: none;">
        <div class="form-group">
          <label for="descripcion">Estado</label>
          <input input type="text" name="descripcion" class="form-control" required>
        </div>

        <input type="hidden" name="insertar" value="insertar" />
        <input class="btn btn-primary" type="submit" value="Guardar" />
      </form>



      <form class="contenedor-tablas col-sm-6" action='tareas_controller.php' method='post' id="insertarTarea" style="display: none;">
        <div class="form-group">
          <label for="fecha_tarea">Fecha Tarea</label>
          <input input type="date" name="fecha_tarea" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="duracion_tarea">Duración</label>
          <input input type="number" name="duracion_tarea" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="desc_tarea">Descripción</label>
          <input input type="text" name="desc_tarea" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="descripcion">Integrante</label>
          <select class="form-control" name="id_integrante">
            <option value="#">Sin integrante</option>
            <?php foreach ($listaIntegrantes as $integrantes) { ?>
              <option value="<?php echo $integrantes->getId_integrante() ?>"><?php echo ($integrantes->getNombre() . " " . $integrantes->getApellido()) ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label for="estado">Estado</label>
          <select class="form-control" name="estado">
            <option value="#">Sin estado</option>
            <?php foreach ($listaEstados as $estados) { ?>
              <option value="<?php echo $estados->getId_estado() ?>"><?php echo ($estados->getDescripcion()) ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label for="observaciones">Observaciones</label>
          <input input type="text" name="observaciones" class="form-control">
        </div>

        <input type='hidden' name='insertar' value='insertar'>
        <input class="btn btn-primary" type='submit' value='Guardar'>
      </form>



      <section class="contenedor-tablas col-sm-10 table-responsive" id="board" style="display: none;">
        <table class="justify-content-center table table-striped table-sm table-bordered table-dark table-hover" cellspacing="0" width="100%">
          <thead>
            <th scope="col">Fecha</th>
            <th scope="col">Descripción</th>
            <th scope="col">Duración</th>
            <th scope="col">Estado</th>
            <th scope="col">Integrante</th>
            <th scope="col">Observaciones</th>
            <th scope="col">Actualizar</th>
            <th scope="col">Eliminar</th>
          </thead>
          <tbody>
            <?php foreach ($listaTareas as $tarea) { ?>
              <tr>
                <td><?php echo $tarea->getFecha_tarea() ?></td>
                <td><?php echo $tarea->getDesc_tarea() ?> </td>
                <td><?php echo $tarea->getDuracion_tarea() ?></td>
                <td><?php foreach ($listaEstados as $estado) {
                      if ($estado->getId_estado() === $tarea->getEstado()) { ?>
                      <?php echo $estado->getDescripcion() ?>
                  <?php }
                    } ?></td>
                <td><?php foreach ($listaIntegrantes as $integrante) {
                      if ($integrante->getId_integrante() === $tarea->getId_integrante()) { ?>
                      <?php echo ($integrante->getNombre() . " " . $integrante->getApellido()) ?>
                  <?php }
                    } ?></td>
                <td><?php echo $tarea->getObservaciones() ?> </td>
                <td><a class="btn btn-outline-primary" href="actualizar.php?id_tarea=<?php echo $tarea->getId_tarea() ?>&accion=a">Actualizar</a> </td>
                <td><a class="btn btn-outline-danger" href="tareas_controller.php?id_tarea=<?php echo $tarea->getId_tarea() ?>&accion=e">Eliminar</a> </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </section>

    </section>
  </div>

  <footer class="pt-3 pl-3">
    Maceira/Ruiz/Salinas/Trillo - 2020
  </footer>

  <script>
    $(".home").waypoint(
      function() {
        $(".home").addClass("slide-animate");
      }, {
        offset: "50%"
      }
    );

    function showForm(selection) {
      let memberBlock = document.getElementById("insertarIntegrantes");
      let statusBlock = document.getElementById("insertarEstado");
      let taskBlock = document.getElementById("insertarTarea");
      let board = document.getElementById("board");
      if (selection === "member") {
        statusBlock.style.display = "none";
        taskBlock.style.display = "none";
        board.style.display = "none";
        if (memberBlock.style.display === "none") {
          memberBlock.style.display = "block";
        } else {
          memberBlock.style.display = "none";
        }
      }
      if (selection === "status") {
        memberBlock.style.display = "none";
        taskBlock.style.display = "none";
        board.style.display = "none";
        if (statusBlock.style.display === "none") {
          statusBlock.style.display = "block";
        } else {
          statusBlock.style.display = "none";
        }
      }
      if (selection === "task") {
        memberBlock.style.display = "none";
        statusBlock.style.display = "none";
        board.style.display = "none";
        if (taskBlock.style.display === "none") {
          taskBlock.style.display = "block";
        } else {
          taskBlock.style.display = "none";
        }
      }
      if (selection === "board") {
        memberBlock.style.display = "none";
        statusBlock.style.display = "none";
        taskBlock.style.display = "none";
        if (board.style.display === "none") {
          board.style.display = "block";
        } else {
          board.style.display = "none";
        }
      }
    }
  </script>
</body>

</html>