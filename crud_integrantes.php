<?php
// incluye la clase Db
require_once('conexion.php');

	class CrudIntegrantes{
		// constructor de la clase
		public function __construct(){}

		// método para insertar, recibe como parámetro un objeto de tipo libintegrantesro
		public function insertar($integrante){
			$db=Db::conectar();
			$insert=$db->prepare('INSERT INTO integrantes values(NULL,:nombre,:apellido,:mail)');
			$insert->bindValue('nombre',$integrante->getNombre());
			$insert->bindValue('apellido',$integrante->getApellido());
			$insert->bindValue('mail',$integrante->getMail());
			$insert->execute();

		}

		// método para mostrar todos los integrantes
		public function mostrar(){
			$db=Db::conectar();
			$listaIntegrantes=[];
			$select=$db->query('SELECT * FROM integrantes');

			foreach($select->fetchAll() as $integrante){
				$myIntegrante= new integrantes();
				$myIntegrante->setId_integrante($integrante['id_integrante']);
				$myIntegrante->setNombre($integrante['nombre']);
				$myIntegrante->setApellido($integrante['apellido']);
				$myIntegrante->setMail($integrante['mail']);
				$listaIntegrantes[]=$myIntegrante;
			}
			return $listaIntegrantes;
		}

		// método para eliminar un integrantes, recibe como parámetro el id del integrantes
		public function eliminar($id_integrante){
			$db=Db::conectar();
			$eliminar=$db->prepare('DELETE FROM integrantes WHERE id_integrante=:id_integrante');
			$eliminar->bindValue('id_integrante',$id_integrante);
			$eliminar->execute();
		}

		// método para buscar un integrantes, recibe como parámetro el id del integrantes
		public function obtenerIntegrante($id_integrante){
			$db=Db::conectar();
			$select=$db->prepare('SELECT * FROM integrantes WHERE id_integrante=:id_integrante');
			$select->bindValue('id_integrante',$id_integrante);
			$select->execute();
			$integrantes=$select->fetch();
			$myIntegrantes = new integrantes();
			$myIntegrante->setId_integrante($integrante['id_integrante']);
			$myIntegrante->setNombre($integrante['nombre']);
			$myIntegrante->setApellido($integrante['apellido']);
			$myIntegrante->setMail($integrante['mail']);
			return $myIntegrantes;
		}

		// método para actualizar un integrantes, recibe como parámetro el integrantes
		public function actualizar($integrantes){
			$db=Db::conectar();
			$actualizar=$db->prepare('UPDATE integrantes SET nombre=:nombre, apellido=:apellido,mail=:mail WHERE id_integrante=:id_integrante');
			$actualizar->bindValue('id_integrante',$integrante->getId_integrante());
			$actualizar->bindValue('nombre',$integrante->getNombre());
			$actualizar->bindValue('apellido',$integrante->getApellido());
			$actualizar->bindValue('mail',$integrante->getMail());
			$actualizar->execute();
		}
	}
?>