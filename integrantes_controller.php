<?php
require_once('crud_integrantes.php');
require_once('integrantes.php');

$crud= new CrudIntegrantes();
$integrante= new integrantes();

	if (isset($_POST['insertar'])) {
		$integrante->setNombre($_POST['nombre']);
		$integrante->setApellido($_POST['apellido']);
		$integrante->setMail($_POST['mail']);

		$crud->insertar($integrante);
		header('Location: index.php');

	}elseif(isset($_POST['actualizar'])){
		$integrante->setId_integrante($_POST['id_integrante']);
		$integrante->setNombre($_POST['nombre']);
		$integrante->setApellido($_POST['apellido']);
		$integrante->setMail($_POST['mail']);
		$crud->actualizar($integrante);
		header('Location: ingresar_integrante.php');

	}elseif ($_GET['accion']=='e') {
		$crud->eliminar($_GET['id_integrante']);
		header('Location: ingresar_integrante.php');		

	}elseif($_GET['accion']=='a'){
		header('Location: ingresar_integrante.php');
	}
?>