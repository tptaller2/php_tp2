<?php
// incluye la clase Db
require_once('conexion.php');

	class CrudEstados{
		// constructor de la clase
		public function __construct(){}

		// método para insertar, recibe como parámetro un objeto de tipo estados
		public function insertar($estados){
			$db=Db::conectar();
			$insert=$db->prepare('INSERT INTO estados values(NULL,:descripcion)');
			$insert->bindValue('descripcion',$estados->getDescripcion());
			$insert->execute();

		}

		// método para mostrar todos los estados
		public function mostrar(){
			$db=Db::conectar();
			$listaEstados=[];
			$select=$db->query('SELECT * FROM estados');

			foreach($select->fetchAll() as $estados){
				$myEstado= new estados();
				$myEstado->setId_estado($estados['id_estado']);
				$myEstado->setDescripcion($estados['descripcion']);
				$listaEstados[]=$myEstado;
			}
			return $listaEstados;
		}

		// método para eliminar un estados, recibe como parámetro el id del estados
		public function eliminar($id_estado){
			$db=Db::conectar();
			$eliminar=$db->prepare('DELETE FROM estados WHERE id_estado=:id_estado');
			$eliminar->bindValue('id_estado',$id_estado);
			$eliminar->execute();
		}

		// método para buscar un estados, recibe como parámetro el id del estados
		public function obtenerEstado($id_estado){
			$db=Db::conectar();
			$select=$db->prepare('SELECT * FROM estados WHERE id_estado=:id_estado');
			$select->bindValue('id_estado',$id_estado);
			$select->execute();
			$estados=$select->fetch();
			$myEstado= new estados();
			$myEstado->setId_estado($estados['id_estado']);
			$myEstado->setDescripcion($estados['descripcion']);
			return $myEstado;
		}

		// método para actualizar un estados, recibe como parámetro el estados
		public function actualizar($estados){
			$db=Db::conectar();
			$actualizar=$db->prepare('UPDATE estados SET descripcion=:descripcion WHERE id_estado=:id_estado');
			$actualizar->bindValue('id_estado',$estados->getId_estado());
			$actualizar->bindValue('descripcion',$estados->getDescripcion());
			$actualizar->execute();
		}
	}
?>